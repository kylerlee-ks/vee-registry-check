﻿using Microsoft.Win32;
using System;
using System.Linq;

namespace RegistryCheck
{
    class Program
    {
        static void Main(string[] args)
        {
            string callableVeeRegistry64 = @"SOFTWARE\WOW6432Node\Classes\CLSID\{603F0662-3B96-11d2-9645-080009CB54BD}\LocalServer32";
            string callableVeeRegistry32 = @"SOFTWARE\Classes\CLSID\{603F0662-3B96-11d2-9645-080009CB54BD}\LocalServer32";
            var registryList = new string[] { callableVeeRegistry32, callableVeeRegistry64 };
            bool hasChanged = false;
            foreach (var reg in registryList)
            {
                // the user must run this console app with Admin permission
                using (RegistryKey callableVeeRegistryKey = Registry.LocalMachine.OpenSubKey(reg, RegistryKeyPermissionCheck.ReadWriteSubTree))
                {
                    if (callableVeeRegistryKey == null)
                        continue;

                    // getting the default name-value pair
                    string value = (string)callableVeeRegistryKey.GetValue("", "");
                    if (!string.IsNullOrWhiteSpace(value) && value.Count(x => x == '"') != 2)
                    {
                        // insert the double quote at the both ends of the vee executable path and update the registry
                        value = value.Replace("\"", string.Empty).Insert(value.IndexOf(".exe") + 4, "\"").Insert(0, "\"");
                        callableVeeRegistryKey.SetValue("", value);
                        hasChanged = true;
                        Console.WriteLine($"Updated default sub-key value: {value}");
                    }
                }
            }
            if (!hasChanged)
                Console.WriteLine("Default sub-key value is updated. No action is needed.");
            Console.Write("Press any key to close this console application.");
            Console.ReadLine();
        }
    }
}
